/*jshint esnext: true, sub: true */

()=>{
    let Papa = require('papaparse');
    let marked = require('marked');
    var config = {};

    let form = document.getElementById('form');
    let inputfile = document.getElementById('inputfile');

    form.addEventListener("submit", (ev)=>{
        ev.preventDefault();
        ev.stopPropagation();

        updateConfig();

        if(!inputfile.files[0]){
            alert('Veuillez sélectionner un fichier.');
            return;
        }

        Papa.parse(inputfile.files[0],{
            delimiter: config.input_separator,
            complete : (results)=>{
                let fixedcsv = Papa.unparse(fixFile(results.data) , {
                    delimiter: ";"
                });

                //TODO : make it a  promise ;)
                download( inputfile.files[0].name.replace(/(\.[^\.]+)$/, '-fixed$1' ) , fixedcsv);
            }
        });
    });

    let fixFile = (data)=>{

        //Get CSV's headers (first line)
        var headers = {};
        data[0].forEach((current, index)=>{
            headers[current] = index;
        });

        //Set new base index for renumbering
        if(config.renumberindex){
            var newindex = config.renumberindex;
        }

        var fixeddata = data.map( (current, index, result)=>{

            if(index===0) return current;

            //Ignore empty lines
            if( current[headers['ID']]===undefined || current[headers['ID']].length===0) return [];

            // Apply bullet list to short description
            if(config.markdownToHTML === true && current[headers['Short description']]){
                current[headers['Short description']] = markdownToHTML(current[headers['Short description']]);
                current[headers['Description']] =  markdownToHTML(current[headers['Description']]) ;
            }

            // Add extension to imgs filenames
            if(config.imgextension.length > 0 && current[headers['Image URLs (x,y,z...)']]){
                current[headers['Image URLs (x,y,z...)']] = fiximageextension(current[headers['Image URLs (x,y,z...)']], config.imgextension);
            }

            //Prepend image filenames with given base URL
            if(config.imgbaseurl.length > 0 && current[headers['Image URLs (x,y,z...)']]){
                current[headers['Image URLs (x,y,z...)']] = prependimagesurl(current[headers['Image URLs (x,y,z...)']], config.imgbaseurl);
            }

            // replace IDs (renumbering)
            if(config.renumberindex){
                current[headers['ID']] = newindex++;
            }

            return current;
        });
        return fixeddata;
    };

    let markdownToHTML = (text) =>{
        return marked(text);//FIXME
    };

    let fiximageextension = (text, extension)=>{
        let urls = text.split(',');
        return urls.map((current)=>{
            return current.trim()+'.'+extension;
        }).join(',');
    };

    let prependimagesurl = (text, imgbaseurl)=>{
        let urls = text.split(',');
        return urls.map((current)=>{
            return imgbaseurl+current.trim();
        }).join(',');
    };

    let updateConfig = ()=>{
        config['markdownToHTML'] = document.getElementById('markdownToHTML').checked;
        config['imgextension'] = document.getElementById('imgextension').value;
        config['imgbaseurl'] = document.getElementById('imgbaseurl').value;
        config['renumberindex'] = ~~document.getElementById('renumberindex').value;
        config['input_separator'] = document.getElementById('input_separator').value;
    };

    let download = (filename, content)=>{
        let link = document.getElementById('downloadlink');
        let blob = new Blob([content]);
        link.setAttribute('href', window.URL.createObjectURL( blob, {type: 'text/plain'}) );
        link.setAttribute('download', filename);
        link.click();
    };

}();
